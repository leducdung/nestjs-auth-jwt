import { Controller, Get, Request, Post, UseGuards,Body , Put , Param, Query , Delete} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import { AuthService } from '../../auth/auth.service';
import { CreateProductDto } from './model/products.dto';
import { ApiTags } from '@nestjs/swagger'
import { ProductsService } from './products.service';
import * as bcrypt from 'bcrypt'

@ApiTags('products')
@Controller('products')
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
) { }

  @UseGuards(JwtAuthGuard)
  @Get('')
  async findMany(
    @Query() query,
    @Request() { req },
  ){
    try {
      const product = await this.productsService.findMany({
        query,
      })

      return product
    } catch (error) {
      throw error
    }
  }


  @UseGuards(JwtAuthGuard)
  @Get(':_id')
  async findOne(
    @Param() _id ,
    @Request() req
  ) {
    try {

      return this.productsService.findOneStoreOwner(_id)

    } catch (error) {
      throw error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('')
  async createOne(
    @Body() createProductDto : CreateProductDto,
    @Request() req
  ) {
    try {
      return this.productsService.createOne({data:createProductDto})
    } catch (error) {
      throw error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':_id')
  async Delete(
    @Param() _id ,
    @Request() req
  ) {
    try {
      return this.productsService.deleteOne(_id)
    } catch (error) {
      throw error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Put(':_id')
  async updateOne(
    @Param() _id ,
    @Body() data ,
    @Request() req
  ) {
    try {
      return this.productsService.updateOne({data,_id})
    } catch (error) {
      throw error
    }
  }

}